#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np

x = np.arange(0, 1.01, 0.01)
y1 = -1/x

plt.plot(x,y1, color="black")
plt.fill_between(x,0,y1, color="red", alpha="0.5")


plt.text(0.5, -50, 'Best response = bot', style='italic',
        bbox={'facecolor':'white', 'alpha':0.5, 'pad':10})

plt.show()