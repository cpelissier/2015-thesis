from __future__ import division
from sympy.solvers import solve
from sympy import *
import numpy as np
import scipy
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D


x = scipy.linspace(-3,3,50)
y = scipy.linspace(-3,3,50)
[x,y] = scipy.meshgrid(x,y)
z = x**2 

fig = plt.figure()
ax = fig.gca(projection='3d')

ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')

ax.plot_surface(x,y,z, alpha=0.3)

x = scipy.linspace(-3,3,50)
z = scipy.linspace(-10,10,50)
[x,z] = scipy.meshgrid(x,z)
y = 1


ax.plot_surface(x,y,z, color='red', alpha=0.3)

x = scipy.linspace(-3,3,50)
y = scipy.linspace(-10,10,50)
[x,y] = scipy.meshgrid(x,y)
z = 2


ax.plot_surface(z,x,y, color='green', alpha=0.3)

"""
theta = np.linspace(-4 * np.pi, 4 * np.pi, 100)
z = np.linspace(-2, 2, 100)
r = z**2 + 1
x = r * np.sin(theta)
y = r * np.cos(theta)
ax.plot(x, y, z, label='parametric curve')
ax.legend()
"""

r = scipy.linspace(2,2,50)
y = scipy.linspace(5, 5, 50)
z = scipy.linspace(-10,10,50)
x = r

ax.plot(y,r,z)


plt.show()





x=Symbol('x')
y=Symbol('y')
z=Symbol('z')

#SOL to above system:

eq_1 = Eq(z, x**2)
eq_2 = Eq(y, 1)
eq_3 = Eq(x,2)

#WOW
sol_1 = solve([eq_1,eq_2,eq_3])
sol_2 = solve([eq_1, eq_3])

print sol_1
print sol_2

#FROM MEETING
#3 different plots of curve + surface

#eq 1

#print solve([Eq(x*y, 0), Eq(x+y, 1)], x, y)

#eq 2

eq_1 = Eq(x*z, 1)
eq_2 = Eq((x-1)*y, 1)
eq_3 = Eq(2*(z*x), 2)

#print solve([eq_1, eq_3])


#Manual testing of Nau eqs.

p=Symbol('p')
q=Symbol('q')
r=Symbol('r')


eq_1 = Eq(q, 1)
eq_2 = Eq(r, 0.75)
eq_3 = Eq(p, 3*q / (q+2))

sol = solve([eq_1, eq_2, eq_3])

print sol



