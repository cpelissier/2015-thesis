import matplotlib
matplotlib.use('webagg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

#p2 degenerate example

fig = plt.figure()
ax = fig.gca(projection='3d')
p = np.arange(0, 1, 0.1)
r = np.arange(0, 1, 0.1)
p,r = np.meshgrid(p,r)
#Convert result from equations into NumPy format. eval is used to get raw data
q = 1
surf = ax.plot_surface(p,r,q, rstride=1, cstride=1,
                       linewidth=0, cmap=cm.coolwarm, antialiased=True)
plt.show()
