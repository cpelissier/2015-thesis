#Candidate no: 51535

from __future__ import division
from sympy.solvers import solve
from sympy import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib.collections import PolyCollection
import re
from utils import *
from nash import *
from fractions import Fraction 

#Delete the comment indicator '#' in any line to run the code for the game in question. Leave only one line uncommented.

#game = "../games/StengelKoller.xml"
#game = "../games/McKelveyMcLennan.xml"
game = "../games/NauCanovasHansen.xml"
#game = "../games/SeltensHorse.xml"

#Constant definitions for graphing ranges.
ARANGE_INTERVAL = 0.10
ARANGE_MIN = 0.0
ARANGE_MAX = 1.0 + ARANGE_INTERVAL

def main():

	#print DEBUG
	p1_raw, p2_raw, p3_raw = import_xml_payoffs(game)
	
	#Payoffs for P1: left -> right, top -> bottom, panel -> panel
	
	#print "\nEnter payoffs for I (separated by a space): "
	
	#p1_raw = map(int,raw_input().split());
	
	p1 = [p1_raw[0:4], p1_raw[4:]]
	
	#Payoffs for P1: left -> right, top -> bottom, panel -> panel
	
	#print "\nEnter payoffs for II (separated by a space): "
	
	#p2_raw = map(int, raw_input().split());
	
	p2 = [p2_raw[0:4],p2_raw[4:]]
	
	#Payoffs for P1: left -> right, top -> bottom, panel -> panel
	
	#print "\nEnter payoffs for III (separated by a space): \n"
	
	#p3_raw = map(int,raw_input().split());
	
	p3 = [p3_raw[0:4], p3_raw[4:]]
	
	print "\nPayoffs for I: " + str(p1) 
	print "Payoffs for II: " + str(p2) 
	print "Payoffs for III: " + str(p3) 
	
	p1,p2,p3 = normalize_game(p1,p2,p3)
	
	print "\nPayoffs for I after normalization: " + str(p1)
	print "Payoffs for II after normalization: " + str(p2) 
	print "Payoffs for III after normalization: " + str(p3) 
	
	
	#Get indifference equations to pass to solver
	p = Symbol('p')
	q = Symbol('q')
	r = Symbol('r')
	
	A = p1[0][0]
	B = p1[0][1]
	C = p1[1][0]
	D = p1[1][1]
	
	a = p2[0][0]
	b = p2[0][2]
	c = p2[1][0]
	d = p2[1][2]
	
	alpha = p3[0][0]
	beta = p3[0][1]
	gamma = p3[0][2]
	delta= p3[0][3]
	
	p1_eq = ((1-q)* (1-r)  * A) + (q * (1-r) * B) + ((1-q) * (r)  * C) + ( q * r * D)
	p1_indifs = solve(p1_eq, q, r)
	
	p2_eq = (p * (1-r) * a) + ((1-p) * (1-r) * b) + (p * r * c) + ((1-p) * r * d) 
	p2_indifs = solve(p2_eq, p, r)
	
	p3_eq = (p * (1-q) * alpha) + (p * q * beta) + ((1-p) * (1-q) * gamma) + ((1-p) * q * delta)
	p3_indifs = solve(p3_eq, p, q)
	
	print "\nI indifferent when: " + str(p1_indifs)
	print "II indifferent when: " + str(p2_indifs)
	print "III indifferent when: " + str(p3_indifs)
	
	#Plot indifference equations in 3D-space 
	eq2plot1(p1_indifs)
	eq2plot2(p2_indifs)
	eq2plot3(p3_indifs)
	
	#Solve for equilibria
	pure = get_pure_equilibria(p1,p2,p3)
	partial = get_partial_equilibria(p1,p2,p3)
	mixed = get_mixed_equilibria(p1_indifs,p2_indifs,p3_indifs)
	
	print "\nPure Equilibria: \n" + str(pure)
	print "\nPartial Equilibria: \n" + str(partial)
	print "\nMixed Equilibria: \n" + str(mixed)
	
	print("\n")


def eq2plot1(eqs):
	
	#Set up figure
	fig = plt.figure()
	ax = fig.gca(projection='3d')
	ax.set_xlabel('p')
	ax.set_ylabel('r')
	ax.set_zlabel('q')
	fig.patch.set_facecolor('white')

	
	for eq in eqs:
		#Splits an equation into left hand side and right hand side, i.e. {p: 3*q} becomes [p, 3*q]
		#TODO: Update with regular exp instead of string.replace()
		eq_arr = sympy2arr(eq) 
		
		if(eq_arr[0] == 'q'):
			p = np.arange(ARANGE_MIN, ARANGE_MAX, ARANGE_INTERVAL)
			r = np.arange(ARANGE_MIN, ARANGE_MAX, ARANGE_INTERVAL)
			p,r = np.meshgrid(p,r)
			#eval function is used to get raw data
			q = eval(eq_arr[1])
			ax.plot_surface(p,r,q, rstride=1, cstride=1, color='blue', alpha=0.7)
		
		elif(eq_arr[0] == 'r'):
			p = np.arange(ARANGE_MIN, ARANGE_MAX, ARANGE_INTERVAL)
			q = np.arange(ARANGE_MIN, ARANGE_MAX, ARANGE_INTERVAL)
			p,q = np.meshgrid(p,q)
			#eval function is used to get raw data
			r = eval(eq_arr[1])
			ax.plot_surface(p,r,q, rstride=1, cstride=1, color='green', alpha=0.7)
			
			
	plt.show()
	
def eq2plot2(eqs):
	
	#Set up figure
	fig = plt.figure()
	ax = fig.gca(projection='3d')
	ax.set_xlabel('p')
	ax.set_ylabel('r')
	ax.set_zlabel('q')
	fig.patch.set_facecolor('white')

	
	for eq in eqs:
		#Splits an equation into left hand side and right hand side, i.e. {p: 3*q} becomes [p, 3*q]
		eq_arr = sympy2arr(eq) 
		
		if(eq_arr[0] == 'p'):
			q = np.arange(ARANGE_MIN, ARANGE_MAX, ARANGE_INTERVAL)
			r = np.arange(ARANGE_MIN, ARANGE_MAX, ARANGE_INTERVAL)
			q,r = np.meshgrid(q,r)
			#eval function is used to get raw data
			p = eval(eq_arr[1])
			ax.plot_surface(p,r,q, rstride=1, cstride=1, color='red', alpha=0.7)
		
		elif(eq_arr[0] == 'r'):
			p = np.arange(ARANGE_MIN, ARANGE_MAX, ARANGE_INTERVAL)
			q = np.arange(ARANGE_MIN, ARANGE_MAX, ARANGE_INTERVAL)
			p,q = np.meshgrid(p,q)
			#eval function is used to get raw data
			r = eval(eq_arr[1])
			ax.plot_surface(p,r,q, rstride=1, cstride=1, color='green', alpha=0.7)
			
			
	plt.show()


def eq2plot3(eqs):
	
	#Set up figure
	fig = plt.figure()
	ax = fig.gca(projection='3d')
	ax.set_xlabel('p')
	ax.set_ylabel('r')
	ax.set_zlabel('q')
	fig.patch.set_facecolor('white')

	
	for eq in eqs:
		#Splits an equation into left hand side and right hand side, i.e. {p: 3*q} becomes [p, 3*q]
		eq_arr = sympy2arr(eq)
		
		if(eq_arr[0] == 'p'):
		
			q = np.arange(ARANGE_MIN, ARANGE_MAX, ARANGE_INTERVAL)
			r = np.arange(ARANGE_MIN, ARANGE_MAX, ARANGE_INTERVAL)
			q,r = np.meshgrid(q,r)
			#eval function is used to get raw data
			p = eval(eq_arr[1])
			
			if('q' in eq_arr[1] ):
				ax.set_xlabel('p')
				ax.set_ylabel('r')
				ax.set_zlabel('q')
				ax.plot_surface(q,r,p, rstride=1, cstride=1, color='red', alpha=0.7)
			else:
				ax.set_xlabel('p')
				ax.set_ylabel('r')
				ax.set_zlabel('q')
				ax.plot_surface(p,r,q, rstride=1, cstride=1, color='red', alpha=0.7)

		elif(eq_arr[0] == 'q'):
			p = np.arange(ARANGE_MIN, ARANGE_MAX, ARANGE_INTERVAL)
			r = np.arange(ARANGE_MIN, ARANGE_MAX, ARANGE_INTERVAL)
			p,r = np.meshgrid(p,r)
			#eval function is used to get raw data
			q = eval(eq_arr[1])
			#ax.plot_surface(p,q,r, rstride=1, cstride=1, color='green', alpha=0.7)
			ax.plot_surface(p,r,q, rstride=1, cstride=1, color='blue', alpha=0.7)
			
			
	plt.show()


if __name__ == "__main__":
	main()	  
	
	