from __future__ import division
from sympy.solvers import solve, nsolve
from sympy import *
from utils import *

#Need a new algorithm? Fix one strategy, check best responses, then ensure fixed player is actually indifferent.
def main():
	p1 = [[0, -1, 0, 0], [-1, 2, 0, 0]]
	p2 = [[0, 0, -1, 0], [0, 0, 3, 0]]
	p3 = [[-2, -2, 1, 0], [0, 0, 0, 0]]
	
	p = Symbol('p')
	q = Symbol('q')
	r = Symbol('r')
	
	A = p1[0][0]
	B = p1[0][1]
	C = p1[1][0]
	D = p1[1][1]
	
	a = p2[0][0]
	b = p2[0][2]
	c = p2[1][0]
	d = p2[1][2]
	
	alpha = p3[0][0]
	beta = p3[0][1]
	gamma = p3[0][2]
	delta= p3[0][3]
	
	#Fix P1 to play T. 
	
	#p2 indifference: (1-r) * 0 + r * 0. => Always indifferent. How to solve this in smypy?
	p2_eq = Eq((1-r) * a + r * c, 0)
	print solve(p2_eq, r)
	p3_eq = Eq((1-q) * alpha + q * beta, q)
	print solve(p3_eq, q)
	
if __name__ == "__main__":
	main()	 