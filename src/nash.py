from __future__ import division
from sympy.solvers import solve
from sympy import *
from utils import *
from fractions import Fraction 
from math import *
import main
#Contains functions to solve for pure, partially mixed, and fully mixed equilibria of 2x2x2 games.

#Get strategy names
p1_strat, p2_strat, p3_strat = import_xml_strategies(main.game)

def get_pure_equilibria(p1,p2,p3):
	#Best response matrices for each player. 1 implies a best response, 0 otherwise.
	p1_br_mat = [[0]*4 for _ in range(2)]
	p2_br_mat = [[0]*4 for _ in range(2)]
	p3_br_mat = [[0]*4 for _ in range(2)]
	
	#Player 1 BR conditions (check BR's along columns)
	#Loop panels
	for i in range(0,2):
		#Compare row values
		for j in range(0,2):
			if(p1[i][j] >= p1[i][j+2]):
				p1_br_mat[i][j] = 1
			if(p1[i][j] <= p1[i][j+2]):
				p1_br_mat[i][j+2] = 1 
				
	#Player 2 BR conditions (check BR's along rows)
	#Loop panels
	for i in range(0,2):
		#Compare column values (increment by 2 instead of 1 when moving to new row)
		for j in range(0,3,2):
			if(p2[i][j] >= p2[i][j+1]):
				p2_br_mat[i][j] = 1
			if(p2[i][j] <= p2[i][j+1]):
				p2_br_mat[i][j+1] = 1 
				
	#Player 3 BR conditions (check BR's along panels)
	#Loop all boxes
	for j in range(0,4):
		if(p3[0][j] >= p3[1][j]):
			p3_br_mat[0][j] = 1
		if(p3[0][j] <= p3[1][j]):
			p3_br_mat[1][j] = 1
			
	#print p1_br_mat
	#print p2_br_mat
	#print p3_br_mat
	
	pure_equilibria = []
	#Loop over best response matrices. If all players have a best response in a specific cell, this is a pure NE.
	for i in range (0,2):
		for j in range (0,4):
			if all(x == 1 for x in (p1_br_mat[i][j],p2_br_mat[i][j],p3_br_mat[i][j])):
				NE = mat2strategy(i,j)
				pure_equilibria.append(NE)
	
	#Convert equilibira from strategy numbers to actual names from the XML file
	
	final_equilibria = []
	
	for [p1_i,p2_i,p3_i] in pure_equilibria:
		final_equilibria.append( [ p1_strat[p1_i],p2_strat[p2_i],p3_strat[p3_i] ] )
	
	return final_equilibria 
		
		
#Takes i,j values referring to panels and cells, respectfully and returns a strategy vector for each player
#e.g. the i,j pair (0,3) corresponds to the third box of the first panel. This is equivalent to playing 
#strategy 1 (B) for player I, strategy 0 (l) for player II, and strategy 0 (L) for player III. Thus, this
#sample input will return the strategy vector [1,0,0]. 
def mat2strategy(i, j):
	p1_strat = p2_strat = p3_strat = 0
	#Index >= 2 refers to bottom row i.e. player I's second strategy
	if(j>=2):
		p1_strat = 1
	#Oddly indexed boxes refer to the second column i.e. player II's second strategy
	if(j%2 != 0):
		p2_strat = 1
	#Second cell refers to player III's second strategy
	if(i==1):
		p3_strat = 1
		
	return [p1_strat, p2_strat, p3_strat]

#Fixes a strategy for each player and finds the corresponding mixed equilibrium for the other two players while keeping the first player indifferent. 
#Finally, checks if the found solution is actually an equilibrium. If not, repeats the process while fixing the previously unused strategy. 
def get_partial_equilibria(p1, p2, p3):
	p = Symbol('p')
	q = Symbol('q')
	r = Symbol('r')
	
	A = p1[0][0]
	B = p1[0][1]
	C = p1[1][0]
	D = p1[1][1]
	
	a = p2[0][0]
	b = p2[0][2]
	c = p2[1][0]
	d = p2[1][2]
	
	alpha = p3[0][0]
	beta = p3[0][1]
	gamma = p3[0][2]
	delta= p3[0][3]
	
	#Solution arrays
	p1_fixed = []
	p2_fixed = []
	p3_fixed = []
	
	#NOTE 20/7/16: Added conditions to handle empty solutions. -inf is used for the payoff of a system with no solution. 
	
	#Fix p1 to play T, solve indifference for II, III
	p2_eq = (1-r) * a + r * c
	r_sol = solve_not_empty(p2_eq)
	p3_eq = (1-q) * alpha + q * beta
	q_sol = solve_not_empty(p3_eq)
	#Ensure we are actually at an equilibrium and III cannot benefit by switching to R. If not, fix R and try again.
	p1_payoff = float("-inf")
	if(q_sol and r_sol):
		p1_payoff = (1-r_sol)*(1-q_sol)*A + (1-r_sol)*q_sol*B + r_sol*(1-q_sol)*C + r_sol*q_sol*D
	
	#Ensure an actually mixed equilibrium (0<x<1 for the two mixed solutions)
	if(0 < r_sol < 1 and 0 < q_sol < 1):
		#Equilibrium found. 
		if(p1_payoff >= 0):
			p1_fixed.append([p1_strat[0], q_sol, r_sol])
		#No equilibrium found. Fix B and try again by comparing new payoff using new probabilities against what payoff would be if we were to switch to T again.
		else:
			p2_eq = (1-r) * b + r * d
			r_sol = solve_not_empty(p2_eq)
			p3_eq = (1-q) * gamma + q * delta
			q_sol = solve_not_empty(p3_eq)
			p1_T_payoff = float("-inf")
			if(q_sol and r_sol):
				p1_T_payoff = (1-r_sol)*(1-q_sol)*A + (1-r_sol)*q_sol*B + r_sol*(1-q_sol)*C + r_sol*q_sol*D
			#Eq. found
			if(p1_T_payoff <= 0):
				p1_fixed.append([p1_strat[1], q_sol, r_sol])
			
	#Fix p2 to play l, solve indifference for I, III 
	p1_eq = (1-r) * A + r * C
	r_sol = solve_not_empty(p1_eq)
	p3_eq = p * alpha + (1-p) * gamma
	p_sol = solve_not_empty(p3_eq)
	#Ensure we are actually at an equilibrium and II cannot benefit by switching to r. If not, fix r and try again.
	p2_payoff = float("-inf")
	if(p_sol and r_sol):
		p2_payoff = (1-r_sol)*p_sol*a + (1-r_sol)*(1-p_sol)*b + r_sol*p_sol*c +  r_sol*(1-p_sol)*d 
	
	#Ensure an actually mixed equilibrium (0<x<1 for the two mixed solutions)
	if(0 < r_sol < 1 and 0 < q_sol < 1):
		#Equilibrium found. 
		if(p2_payoff >= 0):
			p2_fixed.append([p_sol, p2_strat[0], r_sol])
		#No equilibrium found. Fix B and try again by comparing new payoff using new probabilities against what payoff would be if we were to switch to T again.
		else:
			p1_eq = (1-r) * B + r * D
			r_sol = solve_not_empty(p1_eq)
			p3_eq = p * beta + (1-p) * delta
			p_sol = solve_not_empty(p3_eq)
			p2_l_payoff = float("-inf")
			if(p_sol and r_sol):
				p2_l_payoff = (1-r_sol)*p_sol*a + (1-r_sol)*(1-p_sol)*b + r_sol*p_sol*c +  r_sol*(1-p_sol)*d 
			#Eq. found
			if(p2_l_payoff <= 0):
				p2_fixed.append([p1_strat[1], q_sol, r_sol])
	
	
	
	#Fix p3 to play L, solve indifference for I, II
	p1_eq = (1-q) * A + q * B
	q_sol = solve_not_empty(p1_eq)
	p2_eq = p * a + (1-p) * b
	p_sol = solve_not_empty(p2_eq)
	#Ensure we are actually at an equilibrium and III cannot benefit by switching to R. If not, fix R and try again.
	p3_payoff = float("-inf")
	if(p_sol and q_sol):
		p3_payoff = p_sol*(1-q_sol)*alpha + p_sol*q_sol*beta + (1-p_sol)*(1-q_sol)*gamma + (1-p_sol)*q_sol*delta
	
	#Ensure an actually mixed equilibrium (0<x<1 for the two mixed solutions)
	if(0 < r_sol < 1 and 0 < q_sol < 1):
		#Equilibrium found. 
		if(p3_payoff >= 0):
			p3_fixed.append([p_sol, q_sol, p3_strat[0]])
		#No equilibrium found. Fix R and try again by comparing new payoff using new probabilities against what payoff would be if we were to switch to L again.
		else:
			p1_eq = (1-q) *C + q * D
			q_sol = solve_not_empty(p1_eq)
			p2_eq = p * c + (1-p) * d
			p_sol = solve_not_empty(p2_eq)
			p3_L_payoff = float("-inf")
			if(p_sol and q_sol):
				p3_L_payoff = p_sol*(1-q_sol)*alpha + p_sol*q_sol*beta + (1-p_sol)*(1-q_sol)*gamma + (1-p_sol)*q_sol*delta
			#Eq. found
			if(p3_L_payoff <= 0):
				p3_fixed.append([p_sol, q_sol, p3_strat[1]])
		
	if all(not x for x in (p1_fixed, p2_fixed, p3_fixed)):
		return None
		
	return p1_fixed, p2_fixed, p3_fixed
	
#Returns a solution if non-empty, else returns an empty list
def solve_not_empty(eq):
	sol = solve(eq)
	if not sol:
		return []
	return float(sol[0])
			

#Solves system(s) of indifference equations to determine all mixed equilibria. 
def get_mixed_equilibria(p1_indifs, p2_indifs, p3_indifs):
	
	p = Symbol('p')
	q = Symbol('q')
	r = Symbol('r')
	
	eq_arr_p1 = []
	eq_arr_p2 = []
	eq_arr_p3 = []
	
	#final solutions
	sols = []
	
	#Loop over all indifference equations for each player. In the 
	#non-degenerate case, there is one equation per player. 
	for eq_i in p1_indifs:
		for eq_j in p2_indifs:
			for eq_k in p3_indifs:
				#Convert from sympy equations to [LHS, RHS] arrays
				eq1_arr = sympy2arr(eq_i)
				eq2_arr = sympy2arr(eq_j)
				eq3_arr = sympy2arr(eq_k)
				
				#Set up equations and pass to solver
				p1_equation = Eq(eval(eq1_arr[0]), eval(eq1_arr[1]))
				#print p1_equation
				p2_equation = Eq(eval(eq2_arr[0]), eval(eq2_arr[1]))
				#print p2_equation
				p3_equation = Eq(eval(eq3_arr[0]), eval(eq3_arr[1]))
				#print p3_equation
			
				try:
					sol = solve([p1_equation, p2_equation, p3_equation], p, q, r)
					#empty solution, continue
					if not sol:
						dbg("No solution found for this set of parameters, continue to next set")
						continue
					
					#Multiple solutions i.e. in irrational case with +-sqrt(x) solutions. Ensure solutions are valid.
					elif not '{' in str(sol):
						#Ensure a solution 0<=x<=1 for all probabilities
						valid = true
						for i in range(0, len(sol)):
							s = sol[i]
							tmp = str(s)[1:len(str(s))-1].split(',')
							for val in tmp:
								#use eval to create float value
								f = eval(val)
								#Invalid solution, don't add
								if not(f >= 0 and f <= 1):
									valid = false
					 
							if valid:
								dbg("Solution found")
								dbg(sol)
								sols.append(sol[i]) 
					
					#Single solutions to rational systems, append directly 
					else: 
						sols.append(sol)
					
								
				except TypeError:
					dbg("TypeError")
	
	return sols