import xml.etree.ElementTree as ET
import inspect 

#Set to 1 for printing debug info. 
DEBUG = 0

#Parses payoffs of all players, left -> right, top -> bottom, panel -> panel.
def import_xml_payoffs(file):
	tree = ET.parse(file)
	root = tree.getroot()
	
	#find all payoffs in XML file (note: for loop isn't really necessary as only one payoff tag should exist)
	payoff_str = ""
	for payoff in root.iter('payoffs'):
		payoff_str += payoff.text
		
	payoff_arr = payoff_str.split("\n")

	#remove empty entries if any
	for p in payoff_arr:
		if "," not in p:
			payoff_arr.remove(p)
		
	p1_raw = []
	p2_raw = []
	p3_raw = []
	
	for p in payoff_arr:
		individual_payoffs = p.split(",")
		p1_raw.append(int(individual_payoffs[0]))
		p2_raw.append(int(individual_payoffs[1]))
		p3_raw.append(int(individual_payoffs[2]))
	
	return p1_raw, p2_raw, p3_raw
	
def import_xml_strategies(file):
	tree = ET.parse(file)
	root = tree.getroot()
	
	#Strategy name matrix. i index = player number, j index = strategies
	strategy_matrix = [[] for _ in range(3)]
	for strat in root.iter('strategy'):
		#Parse the strategy names so they are separated by a single comma
		tmp = strat.text.replace("{", "").replace("}", "").replace(" ", "").replace("\"", "")
		
		#Add to strategy matrix
		strategy_matrix[int(strat.get('player')) - 1]= list(tmp)
		
	return strategy_matrix[0], strategy_matrix[1], strategy_matrix[2]
	
#Takes payoffs of a game, returns a new game with payoffs normalized such that:
#Bottom row = 0 for P1, Right-hand column = 0 for P2, Second panel = 0 for P3
def normalize_game(p1, p2, p3):
	#Normalize P1
	#Loop panels
	for i in range(0,2):
		#Loop boxes
		for j in range(0,2):
			#p1 bottom row normalized to 0
			p1[i][j] -= p1[i][j+2]
			p1[i][j+2] = 0
	
	#Normalize P2
	#Loop panels
	for i in range(0,2):
		#Loop boxes
		for j in range(0,4):
			#right colums normalized to 0 
			if j % 2 != 0:
				p2[i][j] = 0
			else:
				p2[i][j] -= p2[i][j+1]
				
	#Normalize P3
	#Loop boxes
	for j in range(0,4):
		#right-hand panel normalized to 0
		p3[0][j] -= p3[1][j]
		p3[1][j] = 0;
		
	return p1, p2, p3


#Splits an equation into left hand side and right hand side, i.e. {p: 3*q} becomes the array [p, 3*q]	
def sympy2arr(eq):
	return str(eq).replace('{', '').replace(' ', '').replace('}', '').split(":")
	
#Prints debug info
def dbg(str):
	#TODO: Get name of calling module
	frame = inspect.stack()[1]
	module = inspect.getmodule(frame[0])
	if DEBUG:
		print str



if __name__ == "__main__":
	main()	  