\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Past Work for General-Player Games}{2}
\contentsline {subsection}{\numberline {2.1}Nash Equilibrium and Best Responses}{2}
\contentsline {subsection}{\numberline {2.2}Support Enumeration}{2}
\contentsline {subsection}{\numberline {2.3}Homotopy Algorithms}{3}
\contentsline {section}{\numberline {3}Three-Player Games with Two Strategies per Player}{5}
\contentsline {subsection}{\numberline {3.1}Format}{5}
\contentsline {subsection}{\numberline {3.2}Normalization}{5}
\contentsline {subsection}{\numberline {3.3}Indifference Equations}{7}
\contentsline {section}{\numberline {4}Maximum Number of Equilibria in Generic $2\nobreakspace {}\times \nobreakspace {}2\times \nobreakspace {}2$\nobreakspace {}Games}{10}
\contentsline {subsection}{\numberline {4.1}Pure Equilibria}{10}
\contentsline {subsection}{\numberline {4.2}Partially Mixed Equilibria}{11}
\contentsline {subsection}{\numberline {4.3}Fully Mixed Equilibria}{13}
\contentsline {section}{\numberline {5}Python Module}{15}
\contentsline {subsection}{\numberline {5.1}Game Input and Normalization}{15}
\contentsline {subsection}{\numberline {5.2}Indifference Equation Calculation and Plotting}{16}
\contentsline {subsection}{\numberline {5.3}Pure Equilibrium Solver}{17}
\contentsline {subsection}{\numberline {5.4}Partially Mixed Equilibrium Solver}{18}
\contentsline {subsection}{\numberline {5.5}Mixed Equilibrium Solver}{20}
\contentsline {section}{\numberline {6}Case Study: A Game with Irrational Equilibria}{22}
\contentsline {subsection}{\numberline {6.1}Analysis}{22}
\contentsline {subsection}{\numberline {6.2}\emph {GAMBIT} Results}{23}
\contentsline {subsection}{\numberline {6.3}Python Algorithm Results}{24}
\contentsline {section}{\numberline {7}Case Study: A Generic Game with Nine Equilibria}{26}
\contentsline {subsection}{\numberline {7.1}Analysis}{26}
\contentsline {subsection}{\numberline {7.2}\emph {GAMBIT} Results}{30}
\contentsline {subsection}{\numberline {7.3}Python Algorithm Results}{31}
\contentsline {section}{\numberline {8}Case Study: A Game with a Continuum of Completely Mixed Equilibria}{34}
\contentsline {subsection}{\numberline {8.1}Analysis}{34}
\contentsline {subsection}{\numberline {8.2}\emph {GAMBIT} Results}{35}
\contentsline {subsection}{\numberline {8.3}Python Algorithm Results}{36}
\contentsline {section}{\numberline {9}Case Study: Selten's Horse}{39}
\contentsline {subsection}{\numberline {9.1}Analysis}{39}
\contentsline {subsection}{\numberline {9.2}\emph {GAMBIT} Results}{41}
\contentsline {subsection}{\numberline {9.3}Python Algorithm Results}{43}
\contentsline {section}{\numberline {10}Future Work}{46}
\contentsline {subsection}{\numberline {10.1}Improvements for Complex Cases}{46}
\contentsline {subsection}{\numberline {10.2}Integration into Larger Game Theory Applications}{46}
\contentsline {subsection}{\numberline {10.3}Extensions Beyond $2\times 2\times 2$ Games}{47}
\contentsline {section}{\numberline {11}Conclusion}{49}
\contentsline {section}{\numberline {12}References}{50}
\contentsline {section}{\numberline {A}Appendix}{52}
